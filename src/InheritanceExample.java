
 class Project {

	int ProjectId ;
	String ProjectName;
	
	 void displayEmployee() {
		
		System.out.println("Project Id : "+ProjectId);
		System.out.println("Project Name : "+ProjectName);
		
		System.out.println();
		
	}
	 		
}
class ProjectEmployee extends Project
{
    int EmployeeId ;
    String EmployeeName;
	
void displayProject() {
		
		System.out.println("Employee Id : "+EmployeeId);
		System.out.println("Employee Name : "+EmployeeName);
		
		System.out.println();
		
	}
}
public class InheritanceExample {

	public static void main(String[] args) {
		
		ProjectEmployee employee = new ProjectEmployee();
		employee.ProjectId = 1;
		employee.ProjectName= "Helpdesk";
		employee.EmployeeId =23;
		employee.EmployeeName = "Arya";
		employee.displayProject();
		employee.displayEmployee();
	}

}
