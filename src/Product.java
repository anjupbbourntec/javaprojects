import java.time.LocalDate;

public class Product 
{
	private int id ;
	private String type;
	private double cost;
	private LocalDate createdDate;	
	 public Product( int id, double cost, String type) {
		
		 this.id= id;
		 this.type= type;
		 this.cost=cost;
		 
	}

	public Product() {
		// TODO Auto-generated constructor stub
	}
		public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}
     
}
