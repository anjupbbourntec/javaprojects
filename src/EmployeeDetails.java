import java.util.Scanner;

public class EmployeeDetails {

	public static void main(String[] args) {
						
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Select Your Choice:");
		System.out.println();
		
		System.out.println("1.Enter the details of employee");
		System.out.println("2.Display the details of employee");
		System.out.println();
		
		System.out.println("Enter the choice:");
		int choice = scanner.nextInt();
		select(choice,scanner);
		
	}

	private static void select(int choice, Scanner scanner) {
        
		Employee employee[] = new Employee[1];
		
		switch(choice){
        case 1:
        	System.out.println();
        	
        	System.out.println("Enter the details of employee:");
        	System.out.println();
        	
        	for ( int i =0 ; i<employee.length ; i++ )
    		{
    			employee[i]=new Employee();
    			employee[i].readEmployee(scanner);
    			employee[i].displayEmployee(employee);
    		}        	
            break;         
        case 2:
        	System.out.println();
        	
        	System.out.println("Details of Employee:");
        	System.out.println();
    		for ( int i =0 ; i<employee.length ; i++ )
    		{   			
    			employee[i].displayEmployee(employee);
    		}
            break;         
        default:
        	System.out.println("Please enter a valid input");
        	break;
        }
	}

}
