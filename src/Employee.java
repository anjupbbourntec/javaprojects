import java.util.Scanner;

public class Employee {

	private int Id ;
	private String Name;
	private double PhoneNumber;
	private String Designation;
	
	 public void readEmployee(Scanner scanner) {
		System.out.println("Enter the Id:");
		Id=scanner.nextInt();
		System.out.println("Enter the Name:");
		Name=scanner.next();
		System.out.println("Enter the PhoneNumber:");
		PhoneNumber=scanner.nextDouble();
		System.out.println("Enter the Designation:");
		Designation=scanner.next();
		System.out.println();
		
	}
	
    public void displayEmployee(Employee[] employee) {
		
		System.out.println("Employee Id : "+Id);
		System.out.println("Name : "+Name);
		System.out.println("Phone Number : "+PhoneNumber);
		System.out.println("Designation : "+Designation);
		System.out.println();
		

		
	}
	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public double getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(double phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getDesignation() {
		return Designation;
	}

	public void setDesignation(String designation) {
		Designation = designation;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
