
public class StringToExample {

	public static void main(String[] args) {
		
		Product product1 = new Product(1,200,"FOOD");
		Product product2 = new Product(2,330,"FLOWER");
		
		System.out.println(product1);
		System.out.println();
		System.out.println(product2);
	}

}
