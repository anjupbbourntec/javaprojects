package Arraylist;


import java.util.Scanner;
import java.time.LocalDate;;

public class Product implements Comparable<Product>
{
	private int id ;
	private String type;
	private int cost;
		
	 public Product( int id, int cost, String type) {
		
		 this.id= id;
		 this.type= type;
		 this.cost=cost;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
	

	public void readProduct(Scanner scanner) {
		System.out.println("Enter the Type:");
		type=scanner.next();
		System.out.println("Enter the Id:");
		id=scanner.nextInt();
		System.out.println("Enter the Cost:");
		cost=scanner.nextInt();
		System.out.println();
		
	}	
      
	public String toString() {
		
		return new StringBuffer().append("Id:").append(id).append("\n").append("Type:")
				.append(type).append("\n").append("Cost:").append(cost).append("\n")
				.append("\n").toString();
	}
	public boolean equals(Product product)
	{
		if(type==null)
			return false;
		if(id==product.getId()&&type==product.getType()&&cost==product.getCost())
			return true;
		else
			return false;
	}

public void displayProduct() {
	
	System.out.println("Product Id : "+id);
	System.out.println("Product Type : "+type);
	System.out.println("Product Cost : "+cost);
	System.out.println();
	
}

@Override
public int compareTo(Product obj) {
	
	if(cost<obj.cost)
	   return -1;
	if(cost>obj.cost)
		   return 1;
	else 
		return 0;
				 
}

public String getDetails(Product product) {
	// TODO Auto-generated method stub
	StringBuffer str = new StringBuffer();
	str.append(product.getId());
	str.append(" ,");
	str.append(product.getType());
	str.append(" ,");
	str.append(product.getCost());
	return str.toString();
}
     
}
