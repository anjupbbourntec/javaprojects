package Arraylist;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ProductArrayList {

	public static void main(String[] args) throws IOException {
        
        ArrayList<Product> products= new ArrayList<Product>();

        products.add(new Product(1,230,"type1"));
        products.add(new Product(2,500,"type2"));
        products.add(new Product(3,760,"type3"));
        products.add(new Product(4,330,"type4"));
        
        System.out.println(products);

        for (Product product: products) {
        	
        	String result = product.getDetails(product);   		
    		FileWriter file = new  FileWriter("Example1.csv");
    		file.write(result);
    		file.close();
        }
	}

}
