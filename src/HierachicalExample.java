
class Fruits {
    public void printFruits() { System.out.println("Fruit is the Parent Class"); }
}
 
class Mango extends Fruits {
    public void printMango() { System.out.println("Mango is a Child Class"); }
}
class Apple extends Fruits {
    public void printApple() { System.out.println("Apple is a Child Class"); }
}
  

public class HierachicalExample {

	public static void main(String[] args) {
		
		Mango mango = new Mango();
		mango.printFruits();
		System.out.println();
		
		mango.printMango();
		System.out.println();
		
		Apple apple = new Apple();		
		apple.printApple();


	}

}
