import java.util.Scanner;

public class MenuDriven {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		ProductService product = new ProductService();
		int count=0,choice;
		System.out.println("Select Your Choice:");
		System.out.println();
		
		System.out.println("1.Enter the details of product");
		System.out.println("2.Display the details of product");
		System.out.println("3.Search details");
		System.out.println("4.Sort");
		System.out.println("5.Exit");
		System.out.println();
				
		while(count<product.product.length)
		{
			
			    System.out.println("Enter the choice:");
		        choice = scanner.nextInt();
		        
		    switch(choice){
	        case 1:
	        	product.readProduct();		        		
				break;
	        case 2:
	        	product.displayAll();	
				break;
	        case 3:
	        	product.searchProduct();
	        	break;
	        case 4:
	        	product.compareTo(product);
	        	break;
	        case 5:
	        	System.exit(choice);
		    } 
			
		    
		}	
		}

	}


