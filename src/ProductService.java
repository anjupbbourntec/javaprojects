import java.time.LocalDate;
import java.util.Scanner;

public class ProductService implements Comparable<ProductService> {

	private int id ;
	private String type;
	private double cost;
	private LocalDate createdDate;
	
	ProductService[] product = new ProductService[10];
	Scanner scanner = new Scanner(System.in);
	int count=0;
	
	
	

	public ProductService(int id, double cost, String type) {
		this.id=id;
		this.cost=cost;
		this.type=type;		
	}

	public ProductService() {
		// TODO Auto-generated constructor stub
	}

	public void ProductService1() {
		// TODO Auto-generated constructor stub
	}

		public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}
   
public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}
	public String toString() {
		return (id+" : "+type+" ,cost of "+cost);
	}
	public boolean equalCheck(ProductService product2)
	{
		if(product2==null)
			return false;
		if(this.id==product2.id&&this.type==product2.type&&this.cost==product2.cost)
			return true;
		else
			return false;
	}
	
	
public static void main(String[] args) {
	// TODO Auto-generated method stub

}
public void getProduct(Scanner scanner) {
	
	System.out.println("Enter the Type:");
	type=scanner.next();
	System.out.println("Enter the Id:");
	id=scanner.nextInt();
	System.out.println("Enter the Cost:");
	cost=scanner.nextDouble();
	System.out.println();
//	System.out.println("Enter the Created Date:");
//	String date=scanner.next();
//	this.createdDate = LocalDate.parse(date); 
	
}


public void readProduct() {
	System.out.println();		        	
	System.out.println("Enter the details of product:");
	System.out.println();
	
	System.out.println("Enter the Type:");
	type=scanner.next();
	System.out.println("Enter the Id:");
	id=scanner.nextInt();
	System.out.println("Enter the Cost:");
	cost=scanner.nextDouble();
	System.out.println();
	product[count] = new ProductService(id,cost,type);	
	count++;
}

public void displayProduct(ProductService[] product) {
	
	
	System.out.println("Product Id : "+id);
	System.out.println("Product Type : "+type);
	System.out.println("Product Cost : "+cost);
//	System.out.println("Created Date : "+createdDate);
	System.out.println();
	}
	


public void displayAll()
{
	if(product[0]==null)
		System.out.println("No Data");
	else {
		for(int i=0;i<count;i++)
			product[i].displayProduct(product);
	}
}


public void searchProduct() 
{
	System.out.println("Enter the Product Id for details:");
    int search = scanner.nextInt();	
	if(product[0]==null)
		System.out.println("No Data");
	else {
	for (int i = 0; i < count; i++)
	   {
		   if(product[i].getId()==search)
		 product[i].displayProduct(product);
	   }
	}// TODO Auto-generated method stub
	
}

@Override
public int compareTo(ProductService product) {
	
	if (this.id != product.getId()) {
        return (int) (this.cost - product.getCost());
    }
    return this.type.compareTo(product.getType());
	
}


}
