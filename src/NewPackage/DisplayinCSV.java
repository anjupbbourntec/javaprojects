package NewPackage;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

public class DisplayinCSV {

	public static void main(String[] args) throws IOException {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the details of product:");
		System.out.println();
		
		System.out.println("Enter the Type:");
		String type=scanner.next();
		System.out.println("Enter the Id:");
		int id=scanner.nextInt();
		System.out.println("Enter the Cost:");
		double cost=scanner.nextDouble();
		System.out.println("Enter the Date:");
		String date=scanner.next();
		LocalDate createdDate = LocalDate.parse(date); 
		System.out.println();
		scanner.close();
		Product product = new Product(id,cost,type,createdDate);
		String result = product.getDetails(product);
		
		FileWriter file = new  FileWriter("Example.csv");
		file.write(result);
		file.close();
	}

}
