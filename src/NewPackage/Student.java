package NewPackage;

import java.util.Scanner;

public class Student extends Person {
	
	private int mark;
	
	public Student(String name, int age, Gender gender,int mark) {
		super(name, age, gender);
		this.mark=mark;
	}
	

	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}
	
	public void display() {
		
		System.out.println("Name of student : "+getName());
		System.out.println("Age : "+getAge());
		System.out.println("Mark : "+mark);
		System.out.println("Gender : "+getGender());
		System.out.println();
		
	}
	public void readStudent(Scanner scanner) {
		System.out.println("Enter the Name:");
		setName(scanner.next());
		System.out.println("Enter the Age:");
		setAge(scanner.nextInt());
		System.out.println("Enter the Mark:");
		this.mark=scanner.nextInt();
//		System.out.println("Enter the Gender:");
//		setGender(scanner.Gender());
		System.out.println(); 
		
	}

}
