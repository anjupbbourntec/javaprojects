package NewPackage;

import java.util.Scanner;

public class GenericClass {
	
	static<T> void display(T a)
	{
		System.out.println(a);
	}

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Enter the String:");
		String str=scanner.next();
		System.out.println("Enter the number:");
		int num=scanner.nextInt();
		System.out.println("Enter the next number:");
		double num1=scanner.nextDouble();
		scanner.close();
		
		System.out.println();
		display(str);
		display(num);
		display(num1);

	}

}
