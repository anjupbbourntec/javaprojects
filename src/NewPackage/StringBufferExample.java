package NewPackage;

import java.util.Scanner;

public class StringBufferExample {

	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the string:");
		String str = scanner.next();
        scanner.close();
        boolean result = checkPalindrome(str);
        System.out.println(result);

	}

	private static boolean checkPalindrome(String str) {
		
		StringBuffer str1 = new StringBuffer(str);
		str1.reverse();
		String str2=str1.toString();
		if(str.equals(str2))
			return true;
       else
    	   return false;
		
	}

}
