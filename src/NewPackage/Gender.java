package NewPackage;

public enum Gender {
	
    	Male,
    	Female,
    	Transgender,
    	NotInterested

}
