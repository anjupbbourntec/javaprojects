package NewPackage;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class FileReaderExample {

	public static void main(String[] args) throws IOException 
	{
		FileReader filereader = new FileReader("File.txt");
		Scanner scanner = new Scanner(filereader);
		int num1=scanner.nextInt(); 
		int num2=scanner.nextInt();
		scanner.close();
		int sum=num1+num2;
		
		System.out.println("Sum = "+sum);
		filereader.close();
	}

}
