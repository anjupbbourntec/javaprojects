package NewPackage;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileExample {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the String:");
		String str=scanner.next();
        scanner.close();
        
        try {
			FileWriter file = new FileWriter("FileWriterSample.txt");
			file.write(str);
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
