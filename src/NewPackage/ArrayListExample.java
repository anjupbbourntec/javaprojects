package NewPackage;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListExample {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the First String:");
		String str1=scanner.next();
		System.out.println();
		System.out.println("Enter the Second String:");
		String str2=scanner.next();
		System.out.println();
		System.out.println("Enter the Third String:");
		String str3=scanner.next();
		System.out.println();
		scanner.close();
		
		ArrayList<String> arrayList= new ArrayList<String>(3);
			 arrayList.add(str1);
			 arrayList.add(str2);
			 arrayList.add(str3);
       checkArrayList(arrayList);
	}

	private static void checkArrayList(ArrayList<String> arrayList) {
		
		int count=0;
		for (String arr : arrayList) 
		{
			if(arr.charAt(0)=='a'||arr.charAt(0)=='A')
			{
				System.out.println(arr);
				count++;
			}
		}
		if(count==0)
			System.out.println("No String starts with a or A");
		
		
	}

}
