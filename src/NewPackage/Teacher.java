package NewPackage;

import java.util.Scanner;

public class Teacher extends Person {
	
	
	private double salary;
	
	public Teacher(String name, int age,double salary,Gender gender) {
		super(name, age,gender);
		this.salary = salary;
	}
	

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
public void display() {
		
		System.out.println("Name of teacher : "+getName());
		System.out.println("Age : "+getAge());
		System.out.println("Salary : "+salary);
		System.out.println("Gender : "+getGender());
		System.out.println();
		
	}

public void readTeacher(Scanner scanner) {
	System.out.println("Enter the Name:");
	setName(scanner.next());
	System.out.println("Enter the Age:");
	setAge(scanner.nextInt());
	System.out.println("Enter the Salary:");
	this.salary=scanner.nextDouble();
	//System.out.println("Enter the Gender:");
	//setGender(scanner.);
	System.out.println(); 
	
}
	
	
		 
}
