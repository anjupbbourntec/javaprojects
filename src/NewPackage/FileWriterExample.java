package NewPackage;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileWriterExample {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the first number:");
		int num1=scanner.nextInt();
		System.out.println("Enter the second number:");
		int num2=scanner.nextInt();		
        scanner.close();
        
        try {
			FileWriter file = new FileWriter("File.txt");
			file.write(num1+" "+num2);
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
