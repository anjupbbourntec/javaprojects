
abstract class Book {
	
    String type;
    int cost;
    
	public abstract String getType();
	public abstract String getCost();
      
    public Book(String type,int cost)
    {
        this.type = type;
        this.cost =cost;
    }
}
class Book1 extends Book {
    
    public Book1(String type,int cost)
    {
 
        super(type,cost);
        this.type = type;
        this .cost = cost;
    }

	@Override
	public String getType() {
		
		return "Book type is " + super.type;
        
	}
   public String getCost() {
		
		return "Book cost is " + super.cost;
        
	}
	
}

public class AbstractExample {

	public static void main(String[] args) {
		
		Book s1 = new Book1("Story", 200);  
        System.out.println(s1.getType());
        System.out.println(s1.getCost());
       

	}

}
