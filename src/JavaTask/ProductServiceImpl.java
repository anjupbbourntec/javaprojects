package JavaTask;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Scanner;


public class ProductServiceImpl implements ProductService {

	
	Product[] productArray = new Product[50];
	int count=0;

 public void add(Product product) {
	
	productArray[count] = product;	
	count++;
	if(count>productArray.length)
		System.out.println("Warning : Limit exceeds");

}
	
public void displayAll()
{
	if(productArray[0]==null)
		System.out.println("No Data");
	else 
	{
		for(int i=0;i<count;i++)
			productArray[i].displayProduct();
	}
}

public void sort() {
	 Arrays.sort(productArray,0,count);
	 displayAll();
}

public void searchProduct(LocalDate createdDate) {
	
	int flag=0;
	if(productArray[0]==null)
		System.out.println("No Data");
	else {
	for (int i = 0; i < count; i++)
	   {
		   if(productArray[i].getCreatedDate().isBefore(createdDate))
		 productArray[i].displayProduct();
		   flag=1;
	   }
	}
	
}

public void searchProduct(String type) {
	
	if(productArray[0]==null)
		System.out.println("No Data");
	else {
	for (int i = 0; i < count; i++)
	   {
		   if(productArray[i].getType()==type)
		 productArray[i].displayProduct();
	   }
	
}
}

@Override
public void downLoadDetailsAsCSV() {
	
	String data = null;
	StringBuffer string = new StringBuffer();
	for(int i = 0; i < count; i++)
	{
		string.append(productArray[i].getId()).append(",")
		 .append(productArray[i].getType()).append(",")
		 .append(productArray[i].getCost()).append(",")
		 .append(productArray[i].getCreatedDate()).append(",")
		 .toString();
		
		try {
			FileWriter file = new FileWriter("ProductDetails.csv");
            file.write(data);
            file.close();
		}
	 catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
}


}
