package JavaTask;

import java.time.LocalDate;

/**
* A interface specifies the operations on Product object
*  @author
*/
public interface ProductService {
	
	/**
	* The method will add a Product
	* @param product to be added
	*/
	void add(Product product);
	
	/**
	* The method will display all the products
	*/
	void displayAll();
	
	/**
	* The method will sort the products and display all products
	*/
	void sort();
	
	/**
	* The method will search products and display the product with given date
	 * @param  
	* @param date of the product to be searched
	*/
	void searchProduct(LocalDate createddate);
	
	/**
	* The method will search products and display the product with given id
	* @param id of the product to be searched
	*
	*/
	void searchProduct(String type);
	
	
	/**
	* The method will get the csv string
	*/
	 default String getCSVString(Product product)
	 {
		 
	 return new StringBuffer(product.getId())
			 .append(",")
			 .append(product.getType()).append(",")
			 .append(product.getCost()).append(",")
			 .toString();
	 //csv must contain all the fields
			 
	 }
	
	 /**
		* The method will download the product in details in a CSV file
		*/
	void downLoadDetailsAsCSV();
}
