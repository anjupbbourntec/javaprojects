package JavaTask;
import java.time.LocalDate;
import java.util.Scanner;

public class MenuDriven {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		ProductService productService = new ProductServiceImpl();
		int choice;
		
				
		do
		{
			System.out.println("Select Your Choice:");
			System.out.println();
			
			System.out.println("1.Enter the details of product");
			System.out.println("2.Display the details of product");
			System.out.println("3.Search details");
			System.out.println("4.Sort");
			System.out.println("5.Search with Created Date");
			System.out.println("6.Download Product Details");
			System.out.println("6.Exit");
			System.out.println();
			System.out.println("Enter the choice:");
		    choice = scanner.nextInt();
		        
		    switch(choice){
	        case 1:
	        	
	        	System.out.println();		        	
	        	System.out.println("Enter the details of product:");
	        	System.out.println();
	        	
	        	System.out.println("Enter the Type:");
	        	String type=scanner.next();
	        	System.out.println("Enter the Id:");
	        	int id=scanner.nextInt();
	        	System.out.println("Enter the Cost:");
	        	double cost=scanner.nextDouble();
	        	System.out.println("Enter the Date:");
	        	String date=scanner.next();
	        	LocalDate createdDate = LocalDate.parse(date); 
	        	System.out.println();
	        	Product product = new Product(id,cost,type,createdDate);
	        	productService.add(product);
				break;
	        case 2:
	        	productService.displayAll();	
				break;
	        case 3:
	        	System.out.println("Enter the Product type for details:");
	            String poducttype = scanner.next();	
	        	productService.searchProduct(poducttype);
	        	break;
	        case 4:
	        	productService.sort();
	        	break;
	        case 5:
	        	System.out.println("Enter the Created Date for details:");
	        	LocalDate searchDate = LocalDate.parse(scanner.next()); 
	        	productService.searchProduct(searchDate);
	        	break;
	        case 6:
	        	productService.downLoadDetailsAsCSV();
	        	break;
	        case 7:
	        	System.exit(choice);
		    } 
			
		    
		}while(choice!=7);	
		}

	}


