import java.util.Scanner;

public class MethodSum {

	static double printSum(double ...array) 
	{ 
		int size= array.length;
		double sum=0;
		for (int i=0; i<size ; i++)  
		{ 
			sum=sum+array[i];
		}
		
		return sum;
	} 	
	public static void main(String[] args) {

        double sum = printSum(1,5,2,5);
		System.out.println("Sum= "+sum); 
		double sum1 = printSum(1.2,5,2,5);
		System.out.println("Sum= "+sum1); 

	}

}
