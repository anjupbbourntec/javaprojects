
class A {
    public void printA()
    {
        System.out.print("One");
    }
}
 
class  B extends A {
    public void printB() { System.out.print(" And "); }
}
 
class C extends B {
    public void printC()
    {
        System.out.print("Two");
    }
}
 
public class MultilevelInheritance {

	public static void main(String[] args) {


		C  obj= new C();
		obj.printA();
		obj.printB();
		obj.printC();

	}

}
