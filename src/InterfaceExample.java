

interface ProjectExample
	{
		public void getName(); 
		public void getDepartment(); 
		
	}
	
class Project1 implements ProjectExample
{

	@Override
	public void getName() {
		
		System.out.println("Name : ServiceDesk");
	}

	@Override
	public void getDepartment() {
		
		System.out.println("Department : IT");
		
	}
	
} 
public class InterfaceExample {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ProjectExample project = new Project1();
		project.getDepartment();
		project.getName();

	}

}
