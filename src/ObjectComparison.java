import java.util.Scanner;

public class ObjectComparison {

	public static void main(String[] args) {
		
		Product product1 = new Product(1,200,"FOOD");
		Product product2 = new Product(2,330,"FLOWER");
		
		if(product1.equals(product2))
			System.out.println("Equal");
		else
			System.out.println("Not Equal");
	}

}
