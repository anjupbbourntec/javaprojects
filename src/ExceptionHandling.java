import java.util.Scanner;

public class ExceptionHandling {

	
	public static void main(String[] args) {
		        
        try
        {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter the Project Id:");
            int project=scanner.nextInt();
            System.out.println("Project Id: "+project); 
            scanner.close();
        }
        catch(Exception ex)
        {
        	
        	System.out.println("Invalid Attempt");           
        }
        
	}
        
        
}
